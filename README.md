# Overview

## Submit Jobs from Anywhere

Livy enables programmatic, fault-tolerant, multi-tenant submission of Spark jobs from web/mobile apps (no Spark client needed). So, multiple users can interact with your Spark cluster concurrently and reliably.

## Use Interactive Scala or Python

Livy speaks either Scala or Python, so clients can communicate with your Spark cluster via either language remotely. Also, batch job submissions can be done in Scala, Java, or Python.

## No Code Changes Needed

Don’t worry, no changes to existing programs are needed to use Livy. Just build Livy with Maven, deploy the configuration file to your Spark cluster, and you’re off! Check out Get Started to get going.

## What is Apache Livy?

Apache Livy is a service that enables easy interaction with a Spark cluster over a REST interface. It enables easy submission of Spark jobs or snippets of Spark code, synchronous or asynchronous result retrieval, as well as Spark Context management, all via a simple REST interface or an RPC client library. Apache Livy also simplifies the interaction between Spark and application servers, thus enabling the use of Spark for interactive web/mobile applications. Additional features include:

* Have long running Spark Contexts that can be used for multiple Spark jobs, by multiple clients
* Share cached RDDs or Dataframes across multiple jobs and clients
* Multiple Spark Contexts can be managed simultaneously, and the Spark Contexts run on the cluster (YARN/Mesos) instead of the Livy Server, for good fault tolerance and concurrency
* Jobs can be submitted as precompiled jars, snippets of code or via java/scala client API
* Ensure security via secure authenticated communication


# Usage

To deploy the charm

juju deploy ~spiculecharms/livy

This charm is subordinate and requires a Spark server to connect to. 

To attach it to a Spark server run

juju add-relation livy spark

# Contact Information

## Apache Livy

  - https://livy.incubator.apache.org/
  - https://gitlab.com/spiculedata/juju/livy-charm/issues
  - http://anssr.io



