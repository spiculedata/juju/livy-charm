import os
from charms.reactive import when, when_not, set_flag, endpoint_from_flag
from charmhelpers.core.hookenv import resource_get, status_set, log, open_port, unit_private_ip
from subprocess import check_call, Popen, PIPE



@when_not('livy-charm.installed')
def install_livy_charm():
    #Get the path to the resource
    archive = resource_get("livy")

    #Unzip the software
    cmd = ['unzip', archive, '-d', '/opt']
    check_call(cmd)
    createFolder('/opt/livy/logs')
    set_flag('livy-charm.installed')


@when_not('livy-charm.running')
@when('spark.ready')
@when('livy-charm.installed')
def run_livy_charm(none):
    d = dict(os.environ)
    d["SPARK_HOME"] = '/usr/lib/spark'
    d["HADOOP_CONF_DIR"] = '/etc/hadoop/conf'
    # check_call(['/opt/livy/bin/livy-server', 'start', '&'], shell=True, env=d)
    p = Popen(['/opt/livy/bin/livy-server', 'start'], env=d, stdout = PIPE, stderr = PIPE)
    status_set('active', 'Running')
    set_flag('livy-charm.running')

@when('livy.config.requested')
def ship_config(endpoint):
    endpoint.provide_config(unit_private_ip(), '8998')

@when_not('spark.ready')
def wait_spark():
    status_set('waiting', 'Waiting for Spark')



def createFolder(directory):
    try:
        if not os.path.exists(directory):
            os.makedirs(directory)
    except OSError:
        print ('Error: Creating directory. ' + directory)
